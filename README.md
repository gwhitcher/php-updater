# PHP-Updater
This is a script for updating applications and MYSQL databases from a hosted zip file.  It utilizes PCLZIP.

#Installation
To setup PHP-Updater just adjust the options in config.php to your specific needs.  You can also import the MYSQL file updater.sql to test updating MYSQL.  If you do not use this simply comment out the include to the MYSQL file.
